#!/usr/bin/python

import sys, pygame
import random

pygame.init()

stride = 2
linelen = 32
not_black = (255, 255, 255, 255)
black = (0, 0, 0, 255)

true_maxx = 1024
true_maxy = 768
maxx = true_maxx/stride
maxy = true_maxy/stride

dirs = range(4)
offsets = ((-1, 0), (1, 0), (0, -1), (0, 1))
linelocs = range(1, linelen+1)

def get_pixel(surface, x, y):
    if x < 0 or y < 0 or x >= maxx or y >= maxy:
        return not_black
    return surface.get_at((x*stride, y*stride))

def draw_line(surface, x, y, nx, ny, color):
    return pygame.draw.line(surface, color, (x*stride, y*stride),
                            (nx*stride, ny*stride))

surf = pygame.display.set_mode((true_maxx, true_maxy))

while True:

    surf.fill(black)
    pygame.display.update()

    location_stack = []
    x, y = random.randint(0, maxx-1), random.randint(0, maxy-1)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        dir_list = []
        for i in dirs:
            ox, oy = offsets[i]
            xx, yy = x + ox, y + oy
            if get_pixel(surf, xx, yy) == black:
                dir_list.append(i)
        dirlen = len(dir_list)
        if dirlen > 1:
            location_stack.append((x, y))
        elif dirlen == 0:
            stacklen = len(location_stack)
            if stacklen == 0:
                break
            newpos = random.randint(0, stacklen-1)
            x, y = location_stack[newpos]
            location_stack[newpos] = location_stack[-1]
            location_stack.pop()
            continue

        newdir = random.choice(dir_list)
        ox, oy = offsets[newdir]
        xx, yy = x, y

        for i in linelocs:
            xx += ox
            yy += oy
            if get_pixel(surf, xx, yy) != black:
                break

        newlen = random.randint(1, i-1)
        xx, yy = x + ox*newlen, y + oy*newlen

        red = random.randint(50, 255)
        green = random.randint(50, 255)
        blue = random.randint(50, 255)
        color = (red, green, blue)

        rect = draw_line(surf, x, y, xx, yy, color)

        pygame.display.update(rect)
        x, y = xx, yy

    while True:
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            break
