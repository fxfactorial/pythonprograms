def makebold(fn):
	def wrapped():
		return "<b>" + fn() + "</b>"
	return wrapped


@makebold
def hello():
	return "Hello world"

