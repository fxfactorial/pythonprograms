import time,random

def bubble(array):
   for i in range(-1,len(array)-1):
      for j in range(len(array)-1, i+1, -1):
         if array[j-1]>array[j]: 
            x=array[j-1]
            array[j-1]=array[j]
            array[j]=x

# Fill randomness
print "Creating Randomness"
rand = range(5000)
random.shuffle(rand)
# Sort it
print "Sorting 5000 numbers..."
tim = time.clock()
bubble(rand)
print "Time to sort: "+str(time.clock()-tim)
