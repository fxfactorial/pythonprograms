#!/usr/bin/python

import sys, pygame
import math
import random

pygame.init()

maxx = 1024
maxy = 768

paddle_width = 15
paddle_length = 50

black = (0, 0, 0, 255)
draw_color = (255, 255, 255)

surf = pygame.display.set_mode((maxx, maxy))

x = 0

upward = xrange(0, maxx-1)
downward = xrange( maxx-1, 0, -1)

oldx = -1
oldy = -1

def draw_rect(surf, color, x, y):

    newy = y - paddle_length/2

    if newy < 0:
        newy = 0
    elif newy > maxy - paddle_length:
        newy = maxy - paddle_length

    return pygame.draw.rect(surf, color, (x, newy, paddle_width, paddle_length))

while True:

    for i in upward:

        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        updates = []

        if not (oldx < 0 or oldy < 0):
            rect = draw_rect(surf, black, oldx, oldy)
            updates.append(rect)

        y = i
        rect = draw_rect(surf, draw_color, x, y)
        updates.append(rect)

        oldx = x
        oldy = y

        pygame.display.update(updates)

    for i in downward:

        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        updates = []

        if not (oldx < 0 or oldy < 0):
            rect = draw_rect(surf, black, oldx, oldy)
            updates.append(rect)

        y = i
        rect = draw_rect(surf, draw_color, x, y)
        updates.append(rect)

        oldx = x
        oldy = y

        pygame.display.update(updates)

