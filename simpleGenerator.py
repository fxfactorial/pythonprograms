

d = (4, 2, 1, 5, 6, 9)
def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]


        
def multiply(data):
    for value in data:
        yield value**2
        
def addString(Function):
    def wrapped():
        return "<This Number>" + Function() + "</This Number>"
    return wrapped


@addString
def added():
    return "Hi?"

#def makebold(fn):
#    def wrapped():
#        return "<b>" + fn() + "</b>"

