#!/usr/bin/python

import sys, pygame
import math
import random

pygame.init()

maxx = 512
maxy = 512
centx = maxx/2
centy = maxy/2
radius = min(centx, centy)-1
max_dist = radius*radius
draw_color = (255, 255, 255, 255)
circle_color = (255, 0, 0, 255)
black = (0, 0, 0, 255)
max_count = 10000

dirs = range(4)
offsets = ((-1, 0), (1, 0), (0, -1), (0, 1))

surf = pygame.display.set_mode((maxx, maxy))

rect = pygame.draw.circle(surf, circle_color, (centx, centy), radius+1,1)
pygame.display.update(rect)

surf.set_at((centx, centy), draw_color)
pygame.display.update((centx, centy, 1, 1))

counter = 0
while True:
    angle = random.uniform(0, 2*math.pi)
    x = int(radius * math.cos(angle)) + centx
    y = int(radius * math.sin(angle)) + centy

    contact = False

    while not contact:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()

        update_rect = []

        if surf.get_at((x, y)) != circle_color:
            surf.set_at((x, y), black)
            update_rect.append((x, y, 1, 1))

        dir_list = []

        for i in dirs:
            ox, oy = offsets[i]
            newx, newy = x + ox, y + oy
            distx, disty = newx - centx, newy - centy
            new_dist = distx*distx + disty*disty
            if ( newx >= 0 and newx < maxx and
                 newy >= 0 and newy < maxy and
                 new_dist < max_dist ):
                color = surf.get_at((newx, newy))
                if color == draw_color:
                    contact = True
                    break
                else:
                    dir_list.append(i)

        if contact:
            break

        dir = random.choice(dir_list)
        ox, oy = offsets[dir]

        x += ox
        y += oy

        if surf.get_at((x, y)) != circle_color:
            surf.set_at((x, y), draw_color)
            update_rect.append((x, y, 1, 1))

        if update_rect:
            pygame.display.update(update_rect)

    this_color = surf.get_at((x, y))
    if contact:
        if this_color == circle_color:
            break;
        else:
            surf.set_at((x,y), draw_color)
    else:
        if this_color != circle_color:
            surf.set_at((x,y), black)

    update_rect.append((x, y, 1, 1))

    pygame.display.update(update_rect)

rect = pygame.draw.circle(surf, black, (centx, centy), radius+1,1)
pygame.display.update(rect)

while True:
    event = pygame.event.wait()
    if event.type == pygame.QUIT: sys.exit()
