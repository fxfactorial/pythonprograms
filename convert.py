#!/usr/bin/python

import glob
import subprocess

for f in glob.glob("*.gz"):
    subprocess.check_call(["gunzip", f ])
    subprocess.check_call(["bzip2", "-9v", f[:-3]])
