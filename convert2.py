#!/usr/bin/python

import sys
import os
import optparse
import stat
import glob
import subprocess

# This list contains all of the compression formats that I have access to
# For each format:
#	Options(short form, long form, pretty name of type)
#	Raw extension
#	How to uncompress (name of compression utility, arg to tell it to
#		decompress)
#	How to compress (name of compression utility, arg to tell it to
#		compress with max compression)
#	How to test (name of compression utility, arg to tell it to test)
#
# note: prog test essentially runs "prog -t <file" to make sure that
# weird filenames work.

cplist = ((("-g", "--gzip", "gzip"),	".gz",
           ("gzip","-d"),	("gzip","-9"),	("gzip","-t")),
          (("-b", "--bzip2", "bzip2"),	".bz2",
           ("bzip2","-d"),	("bzip2","-9"),	("bzip2","-t")),
          (("-l", "--lzma", "lzma"),	".lzma",
           ("lzma", "-d"),	("lzma","-9"),	("lzma", "-t")),
          (("-x","--xz", "xz"),		".xz",
           ("xz", "-d"),	("xz", "-9"),	("xz", "-t")))

def status(str):
    sys.stdout.write(str)
    sys.stdout.flush()

def switch_name(name, type, which):
    return name[:-len(cplist[type][1])]+cplist[which][1]

def check_archive(file, type):
    infile = open(file, "rb")
    retval = subprocess.call((cplist[type][4][0],
                              cplist[type][4][1]),
                             stdin=infile)
    infile.close()
    return retval

def convert_archive(file, type, which):
    oldname = file
    newname = switch_name(file, type, which)

    if os.path.exists(newname):
        status("[output file exists, skipping]\n")
        return -1

    oldfile = open(oldname, "rb")
    newfile = open(newname, "wb")

    fromproc = subprocess.Popen([cplist[type][2][0],
                                 cplist[type][2][1]],
                                stdin=oldfile,
                                stdout=subprocess.PIPE)
    toproc = subprocess.Popen([cplist[which][3][0],
                               cplist[which][3][1]],
                              stdin=fromproc.stdout,
                              stdout=newfile)

    retval = toproc.wait()
    ostat = oldfile.close()
    nstat = newfile.close()

    if ostat != None or nstat != None:
        status("[close error, skipping]\n")
        return -1

    if retval != 0:
        status("[error converting, skipping]\n")
        return -1

    return retval

# How many compression formats do I know?
types = len(cplist)

parser = optparse.OptionParser()
for i in range(types):
    parser.add_option(cplist[i][0][0],cplist[i][0][1], dest="which",
                      help="Convert files to "+cplist[i][0][2]+" format",
                      action="store_const", const=i)

(options, args) = parser.parse_args()

newformat = options.which

if newformat == None:
    print "You must choose a compression format"
    parser.print_help()
    sys.exit(1)

files = []

for i in range(types):
    if i != newformat:
        for j in glob.glob("*" + cplist[i][1]):
            files.append((j, i))

for name, type in files:

    status(name + ": test, ")
    result = check_archive(name, type)
    if result:
        status("[test failed, skipping]\n")
        continue

    status("convert, ")
    result = convert_archive(name, type, newformat)
    if result != 0:
        # error printed in sub
        continue

    status("test, ")
    newname = switch_name(name, type, newformat)
    result = check_archive(newname, newformat)
    if result:
        status("[new archive test bad, skipping]\n")
        continue

    status("delete, ")
    status("[not yet], ")
    status("done.\n")
